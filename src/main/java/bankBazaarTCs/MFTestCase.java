package bankBazaarTCs;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankBazaarPages.HomePage;
import wdMethods.BankBazaarProjectMethods;

public class MFTestCase extends BankBazaarProjectMethods {
	@BeforeClass
	public void setData() {
		testCaseName = "CreateMF";
		testCaseDescription ="Create a MutualFund";
		category = "Smoke";
		author= "Ani";
		dataSheetName="MF";
	}
	@Test(dataProvider="fetchData")
	public  void createMF(String firstName)   {
		new HomePage()
		.mouseOverInvestments()
		.selectMutualFunds()
		.clickSearchMutual()
		.selectAge()		
		.selectMonthYr()
		.selectDate()
		.clickContinue()
		.selectIncome()
		.clickContinue()
		.selectBank()
		.enterFirstName(firstName)
		.clickViewMF()
		.getMFSchemaNameandPrice();
		
	}

}
