package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage typeFirstName	(String data) {
		WebElement eleEditFirst = locateElement("id", "updateLeadForm_firstName");
		eleEditFirst.clear();
		type(eleEditFirst, data);
		return this;
	}
	public EditLeadPage typeLastName	(String data) {
		WebElement eleEditLast = locateElement("id", "updateLeadForm_lastName");
		eleEditLast.clear();
		type(eleEditLast, data);
		return this;
	}
	
	public ViewLeadPage clickUpdateLead() {
		WebElement eleUpdateBtn = locateElement("xpath", "//input[@value='Update']");
		click(eleUpdateBtn);
		return new ViewLeadPage(); 
	}
	
}
