package pagefactory_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText="Edit")
	WebElement eleEdit;

	public EditLeadPage clickEditLead() {
		click(eleEdit);
		return new EditLeadPage();
	}

}
