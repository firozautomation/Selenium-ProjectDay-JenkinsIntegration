package pagefactory_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LogInPage extends ProjectMethods{
	
	public LogInPage() {

		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(id="username")
	WebElement eleUserName;
	@FindBy(id="password")
	WebElement elePassword;
	@FindBy(className="decorativeSubmit")
	WebElement eleLogin;	
	

	public LogInPage typeUserName(String data) {
		type(eleUserName, data);
		return this;
	}
	
	public LogInPage typePassword(String data) {
		type(elePassword, data);
		return this;
	}
	
	public HomePage clickLogin() {
		click(eleLogin);
		return new HomePage();
	}
	
}









