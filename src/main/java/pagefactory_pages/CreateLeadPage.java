package pagefactory_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{
	
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="createLeadForm_companyName")
	WebElement eleCompanyName;
	@FindBy(id="createLeadForm_firstName")
	WebElement eleFirstName;
	@FindBy(id="createLeadForm_lastName")
	WebElement eleLastName;
	@FindBy(id="createLeadForm_dataSourceId")
	WebElement eleddSource;
	@FindBy(id="createLeadForm_marketingCampaignId")
	WebElement eleddMkt;
	@FindBy(className="smallSubmit")
	WebElement eleCreateLead;

	public CreateLeadPage typeCompanyName(String data) {
		type(eleCompanyName, data);
		return this;
	}
	
	public CreateLeadPage typeFirstName	(String data) {
		type(eleFirstName, data);
		return this;
	}
	public CreateLeadPage typeLastName	(String data) {
		type(eleLastName, data);
		return this;
	}
	
	public CreateLeadPage selectSource(String data) {
		selectDropDownUsingText(eleddSource, data);
		return this;
	}
	public CreateLeadPage selectMarketId(String data) {
		selectDropDownUsingText(eleddMkt, data);
		return this;
	}
	
	public CreateLeadPage clickCreateLead() {
		click(eleCreateLead);
		return this; 
	}
	
}









