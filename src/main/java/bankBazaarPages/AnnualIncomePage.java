package bankBazaarPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.SeMethods;

public class AnnualIncomePage extends SeMethods {

	public AnnualIncomePage selectIncome()
	{
		WebElement eleslider = locateElement("class", "rangeslider__handle");
		Actions builder=new Actions(driver);
		builder.dragAndDropBy(eleslider, 186, 0).perform();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	public BankActPage clickContinue()
	{
		WebElement eleContinue = locateElement("linktext", "Continue");
		click(eleContinue);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new BankActPage();
	}
	
}
