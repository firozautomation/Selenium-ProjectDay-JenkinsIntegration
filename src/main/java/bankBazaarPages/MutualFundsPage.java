package bankBazaarPages;

import org.openqa.selenium.WebElement;

import wdMethods.SeMethods;

public class MutualFundsPage extends SeMethods{

	public SelectAgePage clickSearchMutual()
	{
		WebElement eleSearchMF = locateElement("linktext", "Search for Mutual Funds");
		click(eleSearchMF);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new SelectAgePage();
	}
}
