package bankBazaarPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.SeMethods;

public class SelectAgePage extends SeMethods{

	public SelectAgePage selectAge()
	{
		WebElement eleslider = locateElement("class", "rangeslider__handle");
		Actions builder=new Actions(driver);
		builder.dragAndDropBy(eleslider, 50, 0).perform();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	public SelectAgePage selectMonthYr()
	{
		WebElement eleMY = locateElement("linktext", "May 1994");
		click(eleMY);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

	public SelectAgePage selectDate()
	{
		WebElement eleDate = locateElement("xpath", "//div[@aria-label ='day-19']");
		click(eleDate);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return this;
	}
	
	public AnnualIncomePage clickContinue()
	{
		WebElement eleContinue = locateElement("linktext", "Continue");
		click(eleContinue);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new AnnualIncomePage();
	}
	
	/*public void verifyDOB()
	{
		WebElement eleDOB =locateElement("xpath", "(//span[@class=\"Calendar_highlight_xftqk\"])[3]");
		String DOB = getText(eleDOB);
		if(D)
	}*/
}
