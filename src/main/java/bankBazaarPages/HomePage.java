package bankBazaarPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.SeMethods;

public class HomePage extends SeMethods{
	
	public HomePage mouseOverInvestments() 
	{
		WebElement eleInvestment = locateElement("xpath", "(//a[text()='INVESTMENTS'])[1]");
		Actions builder=new Actions(driver); //Action syntax
		builder.moveToElement(eleInvestment).pause(3000).perform();	
		
		return this;
	}
	
	public MutualFundsPage selectMutualFunds() 
	{
		WebElement eleMutualFunds = locateElement("xpath", "(//a[text()='Mutual Funds'])[1]");
		
		click(eleMutualFunds);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new MutualFundsPage();
	}
	
}
