package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods {

	@BeforeClass(/*groups="common"*/)
	public void setData() {
		testCaseName = "TC002_EditLead";
		testCaseDescription ="Edit a lead";
		category = "Smoke";
		author= "Ani";
		dataSheetName="EditLeadData";
	}
	@Test(dependsOnMethods="tests.TC001_CreateLead.createLead" ,dataProvider="fetchData")
	public void editLead(String fName, String lName, String cName, String editfName, String editlName) throws InterruptedException
	{
		try {
			new MyHomePage()
			.clickLeads()
			.clickFindLead()
			.typeFindFirstName(fName)
			.typeFindLastName(lName)
			.clickFindLeads()
			.clickFirstLeads()
			.clickEditLead()
			.typeFirstName(editfName)
			.typeLastName(editlName)
			.clickUpdateLead();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

}
