Feature: Create Lead in LeafTap Application
Scenario Outline: Positive way for Creating Lead
Given click on Create Lead Link from Shortcuts
And enter Company Name as <cName>  
And enter First Name as <fName>
And enter Last Name as <lName>
When click on Create Lead button
Then Verify Lead is created succesfully
Examples:
|cName|fName|lName|
|XYZ PVT LTD|HHH|LLL|
|ABC PVT LTD|III|PPP|